package com.project.courierAndroidApp;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainScreenActivity extends Activity{
	
	EditText usernameEditText = null;	
	EditText passwordEditText = null;
	String usernameString = null;
	String passwordString = null;
	
	// single product url
	private static final String url_login = "http://ec2-54-77-11-0.eu-west-1.compute.amazonaws.com:8080/CourierSysAWSv8/loginjson";
	
	// JSON parser class
	JSONParser jsonParser = new JSONParser();

	
	// Progress Dialog
	private ProgressDialog pDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_screen);
		
		usernameEditText = (EditText)findViewById(R.id.l_username);		
		passwordEditText = (EditText)findViewById(R.id.l_password);
				
	}
	
	
	public void startLogin(View view) {
		
		usernameString = usernameEditText.getText().toString().trim();
		passwordString = passwordEditText.getText().toString().trim();		
		
		new Login().execute();
	}
	

	
	
	
	/**
	 * Comite Login in a background thread
	 * */
	class Login extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MainScreenActivity.this);
			pDialog.setMessage("Loggin. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Call loginjson service
		 * */
		protected String doInBackground(String... params) {

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {

				public void run() {
					

					try {

					    	// Building Parameters
					    	List<NameValuePair> params = new ArrayList<NameValuePair>();
					    	params.add(new BasicNameValuePair("uid", usernameString));
					    	params.add(new BasicNameValuePair("psw", passwordString));

					    	// getting  details by making HTTP request
					    	// url use GET request
					    	JSONObject json = jsonParser.makeHttpRequest(
					    			url_login, "GET", params);

					    	// check log for json response
					    	Log.d("Single Product Details", json.toString());
											    						    	
							Integer success = json.getInt("success");

					    	// message if login unsuccesful
					    	if (success == 0) {
					    		Toast.makeText(MainScreenActivity.this, "Wrong credentials. Try again.", Toast.LENGTH_LONG).show();			
					    	}	
					    	
					    	//  start next activity if login is successful
					    	if (success == 1) {
					    		Intent intent = new Intent(MainScreenActivity.this, AllJobsActivity.class);
					    		intent.putExtra("username", usernameString);
					    		startActivity(intent);
					    		finish();
					    	}

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			pDialog.dismiss();
		}
		
  }
	
	
}
