package com.project.courierAndroidApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;



public class AllJobsActivity extends ListActivity implements LocationListener {
	


	// Progress Dialog
	private ProgressDialog pDialog;

	// Creating JSON Parser object
	JSONParser jParser = new JSONParser();

	ArrayList<HashMap<String, String>> jobsList;

	// url to get all jobs list
	private static String url_all_jobs = "http://ec2-54-77-11-0.eu-west-1.compute.amazonaws.com:8080/CourierSysAWSv8/getjobsjson";
	private static String url_stop_courier = "http://ec2-54-77-11-0.eu-west-1.compute.amazonaws.com:8080/CourierSysAWSv8/stopcourierjson";
	
	// lat and lon
	LocationManager locationManager;
	TextView tvLatitude, tvLongitude;
	String provider;
	Location location;
	Float lat = (float) 9999;
	Float lon = (float) 9999;
	

	// products JSONArray
	JSONArray jobs= null;
	
	// username from login activity
	String username = null;
	
	// handler for repeating Thread
    android.os.Handler customHandler = new android.os.Handler();

	

   	/**
	 * On resume from "ShowJobActivity"
	 * */		
	@Override
	protected void onRestart() {
		super.onRestart();
		
		jobsList = new ArrayList<HashMap<String, String>>();
		new LoadAllJobs().execute();
	}
	
	
   	/**
	 * onExit
	 * */	
	public void exitApp(View v) {		
		new Exit().execute();
	}
	
	
   	/**
	 * onCreate
	 * */	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.all_jobs);
		
		// block the screen lock
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// get location
		getLocation();

		// Hashmap for ListView
		jobsList = new ArrayList<HashMap<String, String>>();
	
		// repeater
        customHandler.postDelayed(updateTimerThread, 0);
             
        // get username
        Intent intent = getIntent();
		username = intent.getStringExtra("username");		
	
		
		
    	/**
    	 * Items onClick Listener
    	 * */		
		// Get listview
		ListView lv = getListView();	
		
		// on seleting single job
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
	
				// getting values from selected ListItem
				String jid =  ((TextView) view.findViewById(R.id.jid)).getText().toString();

				// Starting new intent
			    Intent intent = new Intent(AllJobsActivity.this, ShowJobActivity.class);
				// sending job id to next activity
				intent.putExtra("id", jid);
				
				// starting new activity and expecting some response back
				startActivityForResult(intent, 100);
			}
		});
		
		
}
	
	
	

	
	
	
	
	
	
	
	/**
	 * Get Location
	 * */	
	@Override
	public void onLocationChanged(Location location) {
	}


	@Override
	public void onProviderDisabled(String provider) {
		Toast.makeText(AllJobsActivity.this, "Please enable the location tracking.", Toast.LENGTH_SHORT).show();
	
	}


	@Override
	public void onProviderEnabled(String provider) {
	}


	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {		
	}	

    public void getLocation() {
    	
        locationManager =(LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        
	    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this);
	    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 10, this);
    	
	    Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	    Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
 
        long GPSLocationTime = 0;
    	if (null != locationGPS) { GPSLocationTime = locationGPS.getTime(); }

        long NetLocationTime = 0;

    	if (null != locationNet) {
    	   NetLocationTime = locationNet.getTime();
    	}

    	if ( 0 < GPSLocationTime - NetLocationTime ) {
    	   location = locationGPS;
    	}
    	   
    	else {
    	   location = locationNet;
        }
    	
    
       if(location!=null) {   	 
    	   
         lon = (float)location.getLongitude();
         lat = (float)location.getLatitude();    
         
//        // test for location
//		String tempStr = Float.toString(lat) + " " + Float.toString(lon);
// 		Toast.makeText(AllJobsActivity.this, tempStr, Toast.LENGTH_LONG).show();
 		
       } else {
    		Toast.makeText(AllJobsActivity.this, "Location is not set and job's status cannot be updated. Please move towards open area.", Toast.LENGTH_LONG).show();
       }
    	  
   }
	
	
    
    
    
    
    
	/**
	 * Repeating Thread
	 * */
	private Runnable updateTimerThread = new Runnable()
	{
	        public void run()
	        {
	        	getLocation();	

	    		jobsList = new ArrayList<HashMap<String, String>>();
	    		new LoadAllJobs().execute();
	    		
	    		customHandler.postDelayed(this, 30000);
	        }
	};
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Background Async Task to exit and update status
	 * */	
	class Exit extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AllJobsActivity.this);
			pDialog.setMessage("Exiting. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 *  Background thread
		 * */
		protected String doInBackground(String... params) {

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {

				public void run() {
					

					try {

					    	// Building Parameters
					    	List<NameValuePair> params = new ArrayList<NameValuePair>();
					    	params.add(new BasicNameValuePair("uid", username));

					    	JSONObject json = jParser.makeHttpRequest(url_stop_courier, "GET", params);

					    	// check your log for json response
					    	Log.d("Exit:", json.toString());
											    						    	
							Integer success = json.getInt("success");
					    	
					    	if (success == 0) {
					    		Toast.makeText(AllJobsActivity.this, "Connection probllem. Your status remains 'active'", Toast.LENGTH_LONG).show();			
					    	}	

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		
		
		/**
		 * After completing background task Dismiss the progress dialog and stop app
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			pDialog.dismiss();
			
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
			startActivity(intent);
			finish();
			System.exit(0);		
		}
		
  }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	/**
	 * Background Async Task to Load all jobs by making HTTP Request
	 * */
	class LoadAllJobs extends AsyncTask<String, String, String> {

		
		
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AllJobsActivity.this);
			pDialog.setMessage("Loading jobs. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		
		
		
		/**
		 * getting All products from url
		 * */
		@SuppressLint("SimpleDateFormat")
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	    	params.add(new BasicNameValuePair("uid", username));
	    	params.add(new BasicNameValuePair("lat", lat.toString()));
	    	params.add(new BasicNameValuePair("lon", lon.toString()));
			
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_all_jobs, "GET", params);
			
			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());

			try {

					// Getting Array of Jobs
					jobs = json.getJSONArray("jobs");
					
	


					// looping through All Products
					for (int i = 0; i < jobs.length(); i++) {
						
						JSONObject c = jobs.getJSONObject(i);

						// Storing each json item in variable
						String id = c.getString("id");
						String name = c.getString("status");	
						
						// format date
						long timeStamp = Long.parseLong(c.getString("created"));
						Date date = new Date(timeStamp);
						SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                        String created = df.format(date);
				
						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();
						
						// adding each child node to HashMap key => value
						map.put("id", id);
						map.put("status", name);
						map.put("created", created);

						// adding HashList to ArrayList
						jobsList.add(map);
					}

			} 
			catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							AllJobsActivity.this, 
							jobsList,
							R.layout.list_item, 
							new String[] { "id", "status", "created" },
							new int[] { R.id.jid, R.id.status, R.id.created })
					
					
					
					
					
					
					
					{
					   	/**
				    	 * Custom View Item, manage colors
				    	 * */	
				        public View getView(int position, View convertView, ViewGroup parent) {
				                View view = super.getView(position, convertView, parent);
				                
			                    TextView created = (TextView) view.findViewById(R.id.created);
			                    TextView status = (TextView) view.findViewById(R.id.status);
			                    
			                    String statusText = (String) status.getText();
			                    
			                    if (statusText.equals("Sent to Courier")) {			
				                    created.setTextColor(Color.RED);
  				                    status.setTextColor(Color.RED); 
  				                    
  				                    status.setText("NEW");
			                    }		
			                    
			                    if (statusText.equals("Approved")) {			
			                        created.setTextColor(Color.GREEN);
				                    status.setTextColor(Color.GREEN); 
				                }	
			                    
			                    if (statusText.equals("Collected")) {			
			                        created.setTextColor(Color.YELLOW);
				                    status.setTextColor(Color.YELLOW); 
				                }

				              return view;

				        };
				    };
					
					
					
					// updating listview					
					setListAdapter(adapter);

				}
			});

		}

	}

}