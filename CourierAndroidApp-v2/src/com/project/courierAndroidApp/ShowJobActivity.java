package com.project.courierAndroidApp;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ShowJobActivity extends Activity {
	
	// define view elements
	TextView o_name = null;
	TextView o_surname = null;
	TextView o_email = null;
	TextView o_phone = null;
	
	TextView c_name = null;
	TextView c_address1 = null;
	TextView c_address2 = null;
	TextView c_postcode = null;
	TextView c_notes = null;	
	
	TextView d_name = null;
	TextView d_address1 = null;
	TextView d_address2 = null;
	TextView d_postcode = null;
	TextView d_notes = null;
	
	TextView j_price = null;
	TextView j_notes = null;
	
	// services
	private static final String url_job_detials = "http://ec2-54-77-11-0.eu-west-1.compute.amazonaws.com:8080/CourierSysAWSv8/getdetailsjson";
	private static final String url_update_courier_status = "http://ec2-54-77-11-0.eu-west-1.compute.amazonaws.com:8080/CourierSysAWSv8/updatestatusjson";
	
	// JSON parser class
	JSONParser jsonParser = new JSONParser();
	
	String jid;
	String jstatus;
	
	// Progress Dialog
	private ProgressDialog pDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_job);
		
		// get parameter from previous activity
	    Intent intent = getIntent();
		jid = intent.getStringExtra("id");
		
        // set view element variables
		o_name = (TextView)findViewById(R.id.o_name);
		o_surname = (TextView)findViewById(R.id.o_surname);
		o_email = (TextView)findViewById(R.id.o_email);
		o_phone = (TextView)findViewById(R.id.o_phone);
		
		c_name = (TextView)findViewById(R.id.c_name);
		c_address1 = (TextView)findViewById(R.id.c_address1);
		c_address2 = (TextView)findViewById(R.id.c_address2);
		c_postcode = (TextView)findViewById(R.id.c_postcode);
		c_notes = (TextView)findViewById(R.id.c_notes);	
		
		d_name = (TextView)findViewById(R.id.d_name);
		d_address1 = (TextView)findViewById(R.id.d_address1);
		d_address2 = (TextView)findViewById(R.id.d_address2);
		d_postcode = (TextView)findViewById(R.id.d_postcode);
		d_notes = (TextView)findViewById(R.id.d_notes);
		
		j_price = (TextView)findViewById(R.id.j_price);
		j_notes = (TextView)findViewById(R.id.j_notes);
		
		
		// execute background thread
		new GetJobDetails().execute();
		
	}
	
	//define call functions acording on job's status
	public void rejected(View v) {
		jstatus = "Rejected";
		new UpdateJobStatus().execute();
	}	
	
	public void approved(View v) {
		jstatus = "Approved";
		new UpdateJobStatus().execute();	
	}
	
	public void collected(View v) {
		jstatus = "Collected";
		new UpdateJobStatus().execute();	
	}	
	
	public void delivered(View v) {
		jstatus = "Delivered";
		new UpdateJobStatus().execute();	
	}

	
	
	
	
	
	
	
	/**
	 * Update details in background parallel main thread
	 * */
	class GetJobDetails extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ShowJobActivity.this);
			pDialog.setMessage("Loading job details. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... params) {

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					

					try {

					    	// Building Parameters
					    	List<NameValuePair> params = new ArrayList<NameValuePair>();
					    	params.add(new BasicNameValuePair("id", jid));

					    	// getting details by making HTTP request
					    	// url will use GET request
					    	JSONObject json = jsonParser.makeHttpRequest(
								url_job_detials, "GET", params);

					    	// check your log for json response
					    	Log.d("Single Product Details", json.toString());
											    	
					    	JSONObject user = json.getJSONObject("user");
					    	
					    	//update view objects
					      	o_name.setText(user.getString("name"));
					      	o_surname.setText(user.getString("surname"));
					      	o_phone.setText(user.getString("email"));
					      	o_email.setText(user.getString("phone"));
					      	
					    	JSONObject sendfrom = json.getJSONObject("sendfrom");

					      	c_name.setText(sendfrom.getString("name"));
					      	c_address1.setText(sendfrom.getString("address1"));
					      	c_address2.setText(sendfrom.getString("address2"));
					      	c_postcode.setText(sendfrom.getString("postcode"));
					      	c_notes.setText(sendfrom.getString("notes"));	
					      	
					    	JSONObject sendto= json.getJSONObject("sendto");

					      	d_name.setText(sendto.getString("name"));
					      	d_address1.setText(sendto.getString("address1"));
					      	d_address2.setText(sendto.getString("address2"));
					      	d_postcode.setText(sendto.getString("postcode"));
					      	d_notes.setText(sendto.getString("notes"));
					      	
					    	JSONObject job = json.getJSONObject("job");
					    	
					      	j_price.setText(job.getString("price"));
					      	j_notes.setText(job.getString("notes"));
					      	
					      	// show correct buttons					      	
					      	if (job.getString("status").equals("Approved")) {
								   findViewById(R.id.collected).setVisibility(View.VISIBLE);
							}	
					      	
					      	if (job.getString("status").equals("Sent to Courier")) {
								   findViewById(R.id.rejected).setVisibility(View.VISIBLE);
								   findViewById(R.id.approved).setVisibility(View.VISIBLE);
						    }
					      	
					      	if (job.getString("status").equals("Collected")) {
								   findViewById(R.id.delivered).setVisibility(View.VISIBLE);
						    }	
							   

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			pDialog.dismiss();
		}
		
  }
	
	
	
	
	
	
	
	
	class UpdateJobStatus extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ShowJobActivity.this);
			pDialog.setMessage("Updating job. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... params) {

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {

				public void run() {
					

					try {

					    	// Building Parameters
					    	List<NameValuePair> params = new ArrayList<NameValuePair>();
					    	params.add(new BasicNameValuePair("id", jid));
					    	params.add(new BasicNameValuePair("status", jstatus));

					    	// getting details by making HTTP request
					    	//  url will use GET request
					    	JSONObject json = jsonParser.makeHttpRequest(
					    			url_update_courier_status, "GET", params);

					    	// check your log for json response
					    	Log.d("Single Product Details", json.toString());
											    						    	
							Integer success = json.getInt("success");

					    	
					    	if (success == 0) {
					    		Toast.makeText(ShowJobActivity.this, json.getString("message"), Toast.LENGTH_LONG).show();		
					    	}	
					    	
					    	if (success == 1) {
					    		Toast.makeText(ShowJobActivity.this, json.getString("message"), Toast.LENGTH_SHORT).show();		
					    		finish();
					    	}

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			pDialog.dismiss();
		}
		
  }
	
	
	
	

	
}